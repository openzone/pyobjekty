#!/usr/bin/env python3
# Soubor:  kameny.py
# Datum:   06.11.2018 10:01
# Autor:   Marek Nožka, nozka <@t> spseol <d.t> cz
# Licence: GNU/GPL
############################################################################
import pyglet
import random
from random import randint
import glob


meteority = glob.glob("img/meteor*")
print(meteority)

# from pyglet.window.key import LEFT, RIGHT, UP, DOWN, LCTRL
# from pyglet.window.mouse import LEFT as MouseLEFT

window = pyglet.window.Window(width=800, height=600)
batch = pyglet.graphics.Batch()  # pro optimalizované vyreslování objektů


class SpaceObject(pyglet.sprite.Sprite):
    def __init__(self, img_file, x=0, y=0, speedx=0, speedy=0):
        img = pyglet.image.load(img_file)
        img.anchor_x = img.width // 2
        img.anchor_y = img.height // 2
        super().__init__(img, batch=batch)
        self.x = x
        self.y = y
        self.speedx = speedx
        self.speedy = speedy

    def move(self):
        self.x += self.speedx
        self.y += self.speedy
        if (self.x - self.width / 2 < 0) or (self.x + self.width / 2 > window.width):
            self.speedx = -self.speedx
        if (self.y - self.height / 2 < 0) or (self.y + self.height / 2 > window.height):
            self.speedy = -self.speedy


@window.event
def on_draw():
    window.clear()
    batch.draw()


@window.event
def on_key_press(sym, mod):
    print(sym, mod)


@window.event
def on_key_release(sym, mod):
    print(sym, mod)


@window.event
def on_mouse_press(x, y, button, mod):
    print(x, y, button)


def tick(dt):
    for obj in objekty:
        obj.move()


def pridej_sutr(dt):
    objekty.append(
        SpaceObject(
            random.choice(meteority),
            randint(200, 400),
            randint(200, 300),
            randint(-5, 5),
            randint(-5, 5),
        )
    )


objekty = []
objekty.append(SpaceObject("img/meteorGrey_big1.png", 100, 100, 2, 4))
objekty.append(SpaceObject("img/ship.png", 200, 50, 3, 8))

pyglet.clock.schedule_interval(tick, 1 / 30)
pyglet.clock.schedule_interval(pridej_sutr, 1)

pyglet.app.run()
