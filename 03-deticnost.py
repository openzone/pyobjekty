#!/usr/bin/env python3
# Soubor:  kameny.py
# Datum:   06.11.2018 10:01
# Autor:   Marek Nožka, nozka <@t> spseol <d.t> cz
# Licence: GNU/GPL
############################################################################
import pyglet
from glob import glob
from random import randint, choice


# from pyglet.window.key import LEFT, RIGHT, UP, DOWN, LCTRL
# from pyglet.window.mouse import LEFT as MouseLEFT

import pyglet.window.key
import pyglet.window.mouse

window = pyglet.window.Window(width=800, height=600)
batch = pyglet.graphics.Batch()  # pro optimalizované vyreslování objektů


class SpaceObject(pyglet.sprite.Sprite):
    def __init__(self, img_path, speed_x=0, speed_y=0):
        self.img = pyglet.image.load(img_path)
        self.img.anchor_x = self.img.width // 2
        self.img.anchor_y = self.img.height // 2

        # self.sprite = pyglet.sprite.Sprite(self.img, batch=batch)
        super().__init__(self.img, batch=batch)

        self.speed_x = speed_x
        self.speed_y = speed_y

    def move(self, dt):
        self.x += self.speed_x * dt
        self.y += self.speed_y * dt


class Shuter(SpaceObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.x = 400
        self.y = 300

    def move(self, dt):
        self.x += self.speed_x * dt * 3
        self.y += self.speed_y * dt * 3

        if (self.x - self.width / 2 < 0) or (self.x + self.width / 2 > window.width):
            self.speed_x = -self.speed_x
        if (self.y - self.height / 2 < 0) or (self.y + self.height / 2 > window.height):
            self.speed_y = -self.speed_y


@window.event
def on_draw():
    window.clear()
    batch.draw()


@window.event
def on_key_press(sym, mod):
    print(sym, mod)


@window.event
def on_key_release(sym, mod):
    print(sym, mod)


@window.event
def on_mouse_press(x, y, button, mod):
    print(x, y, button)
    ship.x = x
    ship.y = y


def tick(dt):
    ship.move(dt)
    for o in shuter:
        o.move(dt)


ship = SpaceObject("img/ship.png", 30, 60)
shuter = []
for i in range(33):
    shuter.append(
        Shuter(choice(glob("img/meteor*")), randint(-100, 100), randint(-100, 100))
    )


print(glob("img/meteor*"))

# funkce tick se spustí 30x za sekundu
pyglet.clock.schedule_interval(tick, 1 / 30)

# nekonečná smyčka ve které se čeká na události, které se následně obsluhují
pyglet.app.run()
