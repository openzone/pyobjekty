
def dekorator(f):
    def obal(*args, **kwargs):
        vysledek = f(*args, **kwargs)
        return vysledek + 10
    return obal


@dekorator
def jedna(x):
    print(jedna.__name__)
    return x+1


print(jedna(10))
print(jedna(77))